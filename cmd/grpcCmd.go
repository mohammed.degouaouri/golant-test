package cmd

import (
	"github.com/spf13/cobra"
	grpc_server "gitlab.com/golang-test/grpc_tech/server"
)

var grpcPort int = 50001
var grpcCmd = &cobra.Command{
	Use:   "grpc",
	Short: "Starts the grpc process",
	Long:  `Starts the grpc process`,
	Run: func(cmd *cobra.Command, args []string) {
		grpc_server.NewTestGrpsServer(&grpcPort)
	},
}

func init() {
	grpcCmd.PersistentFlags().IntVarP(&grpcPort, "port", "p", grpcPort, "Grpc port")
}
