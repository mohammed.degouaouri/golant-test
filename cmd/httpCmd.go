package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
	http_tech "gitlab.com/golang-test/http"
)

var httpPort int = 8080
var host string = "0.0.0.0"
var httpCmd = &cobra.Command{
	Use:   "http",
	Short: "Starts the http process",
	Long:  `Starts the http process`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("Hello")
		http_tech.NewHttpServer(host, httpPort, grpcPort)
	},
}

func init() {
	httpCmd.PersistentFlags().IntVarP(&httpPort, "port", "p", httpPort, "Http port")
	httpCmd.PersistentFlags().IntVarP(&grpcPort, "grpc", "g", grpcPort, "grpc port")

}
