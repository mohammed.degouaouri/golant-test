package http_tech

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"sync"

	"github.com/fsnotify/fsnotify"
)

func watchFile(filepath string, dataChan chan string) {
	fmt.Println("Calling watchFile")
	_, err := tail(filepath, dataChan)
	if err != nil {
		fmt.Println("Tailing error: ", err)
		return
	}
	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		log.Fatal(err)
	}
	defer watcher.Close()

	// WaitGroup to wait for goroutines to finish
	var wg sync.WaitGroup

	// Start a goroutine to handle file events
	wg.Add(1)
	go func() {
		defer wg.Done()
		for {
			select {
			case event, ok := <-watcher.Events:
				if !ok {
					return
				}
				if event.Has(fsnotify.Write) {
					fmt.Println("File updated:", event.Name)
					// watcher := bufio.NewReader(file)
					// for {
					// line, err := watcher.ReadSlice('\n')
					// 	if err != nil {
					// 		fmt.Println("Last lines error", err)
					// 		break
					// 	}
					// 	fmt.Printf("Live tailing: %s\n", line)
					// 	data <- string(line)
					// }
				}
			case err, ok := <-watcher.Errors:
				if !ok {
					return
				}
				fmt.Println("Error:", err)
			}
		}
	}()

	// Walk through the directory to add existing files to the watcher
	err = watcher.Add(filepath)
	if err != nil {
		fmt.Println("Error adding", filepath, "to watcher:", err)
	}
	// Wait for goroutine to finish
	wg.Wait()
}

func tail(filepath string, dataChan chan string) (*os.File, error) {
	file, err := os.Open(filepath)
	if err != nil {
		return nil, err
	}

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		// fmt.Printf("Archive: %s\n", scanner.Text())
		dataChan <- scanner.Text()
	}
	watcher := bufio.NewReader(file)
	for {

		line, err := watcher.ReadSlice('\n')
		if err != nil {
			break
		}
		fmt.Printf("live tailing: %s", line)
		dataChan <- string(line)

	}
	return file, err
}
