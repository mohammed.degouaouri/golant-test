package http_tech

import (
	"context"
	"fmt"
	"io"
	"log"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	"github.com/gorilla/websocket"
	grpc_tech "gitlab.com/golang-test/grpc_tech/proto"
	grpcpb "gitlab.com/golang-test/grpc_tech/proto"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

var upgrader = websocket.Upgrader{
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

func (httpServer *TechHttpServer) healthChecksHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	service := vars["service"]
	// // Set up a connection to the server.
	addr := fmt.Sprintf("%s:%d", httpServer.Host, httpServer.GrpcPort)
	httpReponse := &HealthCheckHttpReponse{}
	conn, err := grpc.Dial(addr, grpc.WithTransportCredentials(insecure.NewCredentials()))
	defer conn.Close()
	if err != nil {
		httpReponse.Error = err.Error()
		log.Default().Printf("Did not connect: %v", err)
	}
	c := grpcpb.NewTestServiceClient(conn)

	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	grpcResp, err := c.GetHealthChecks(ctx, &grpcpb.HealthCheckRequest{
		Service: service,
	})
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	httpReponse.Data = grpc_tech.HealthCheckInfo{
		Service: grpcResp.Service,
		Check:   grpcResp.Check,
	}
	err = httpReponse.Jsonify(w)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (httpServer *TechHttpServer) websocketHandler(w http.ResponseWriter, r *http.Request) {
	wsConn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer wsConn.Close()
	addr := fmt.Sprintf("%s:%d", httpServer.Host, httpServer.GrpcPort)
	fmt.Println(addr)
	conn, err := grpc.Dial(addr, grpc.WithTransportCredentials(insecure.NewCredentials()))
	defer conn.Close()
	if err != nil {
		log.Default().Printf("Did not connect: %v", err)
		return
	}
	c := grpcpb.NewTestServiceClient(conn)

	ctx := context.Background()
	for {
		// // Read message from the client
		_, filepath, err := wsConn.ReadMessage()
		if err != nil {
			fmt.Println(err)
			return
		}
		fmt.Printf("Received data: %s\n", filepath)
		stream, err := c.StreamLogs(ctx, &grpcpb.StreamLogsRequest{
			File: string(filepath),
		})
		for {
			resp, err := stream.Recv()
			if err == io.EOF {
				return
			} else if err == nil {
				err = wsConn.WriteMessage(websocket.TextMessage, []byte(resp.Line))
				if err != nil {
					fmt.Println("Socket error", err)
					return
				}
			}

			if err != nil {
				fmt.Println("Socket error", err)
				return
			}

		}

	}
}

func NewHttpServer(host string, port int, grpcPort int) {
	// Create a new router using Gorilla mux
	router := mux.NewRouter()
	techHttpServer := NewTechHttpServer(host, port, grpcPort)

	// Register a handler function for the "/hc/{name}" route
	router.HandleFunc("/hc/{service}", techHttpServer.healthChecksHandler).Methods("GET")
	router.HandleFunc("/ws", techHttpServer.websocketHandler).Methods("GET")

	// Set the port to listen on
	addr := fmt.Sprintf(":%d", port)

	// Start the HTTP server
	fmt.Printf("Http server is running on %s\n", addr)
	err := http.ListenAndServe(addr, router)
	if err != nil {
		fmt.Printf("Error starting server: %v\n", err)
	}
}
