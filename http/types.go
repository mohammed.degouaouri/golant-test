package http_tech

import (
	"encoding/json"
	"errors"
	"net/http"

	grpc_tech "gitlab.com/golang-test/grpc_tech/proto"
)

type TechHttpServer struct {
	Host     string
	Port     int
	GrpcPort int
}

type GetHealthChecksDto struct {
	service string
}

type JsonResponse interface {
	Jsonify(w http.ResponseWriter) error
}

type HealthCheckHttpReponse struct {
	Data  grpc_tech.HealthCheckInfo `json:"data"`
	Error string                    `json:"error"`
}

func (hcReponse *HealthCheckHttpReponse) Jsonify(w http.ResponseWriter) error {
	data, err := json.Marshal(hcReponse)
	// Send response
	if err != nil {
		return errors.New("Error encoding JSON")
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(data)
	return nil
}

func NewTechHttpServer(host string, port int, grpcPort int) *TechHttpServer {
	return &TechHttpServer{
		Host:     host,
		Port:     port,
		GrpcPort: grpcPort,
	}
}
