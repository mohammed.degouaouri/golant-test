package watcher

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"sync"

	"github.com/fsnotify/fsnotify"
)

func WatchFile(filepath string, dataChan chan string, errChan chan error) {
	file, err := Tail(filepath, dataChan, errChan)
	if err != nil {
		fmt.Println("Tailing error: ", err)
		return
	}
	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		log.Fatal(err)
	}
	defer watcher.Close()

	// WaitGroup to wait for goroutines to finish
	var wg sync.WaitGroup

	// Start a goroutine to handle file events
	wg.Add(1)
	go func() {
		defer wg.Done()
		for {
			select {
			case event, ok := <-watcher.Events:
				if !ok {
					return
				}
				if event.Has(fsnotify.Write) {
					log.Default().Println("File updated: ", event.Name)
					watcher := bufio.NewReader(file)
					for {
						line, err := watcher.ReadSlice('\n')
						if err != nil {
							fmt.Println("Last lines error", err)
							errChan <- err
							break
						}
						fmt.Printf("Live tailing: %s\n", line)
						dataChan <- string(line)
					}
				}
			case err, ok := <-watcher.Errors:
				if !ok {
					return
				}
				fmt.Println("Error:", err)
			}
		}
	}()

	// Walk through the directory to add existing files to the watcher
	err = watcher.Add(filepath)
	if err != nil {
		fmt.Println("Error adding", filepath, "to watcher:", err)
	}
	// Wait for goroutine to finish
	wg.Wait()
}

func Tail(filepath string, dataChan chan string, errChan chan error) (*os.File, error) {
	file, err := os.Open(filepath)
	if err != nil {
		fmt.Println("Open Error", err)
		return nil, err
	}

	watcher := bufio.NewReader(file)
	for {
		fmt.Printf("Before readslice\n")

		line, err := watcher.ReadSlice('\n')
		if err != nil {
			fmt.Println("Error tail: ", err)
			errChan <- err
			break
		}
		fmt.Printf("Live tailing: %s\n", line)
		dataChan <- string(line)

	}
	return file, err
}
