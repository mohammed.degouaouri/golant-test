# Golant Test
Http server and gRPC server implementation as specified
## Run Http server
```bash
go run main.go http -p 8000
```
## Run gRPC server

```bash
go run main.go grpc -p 50001
```

Use the following command for
```bash
go run main.go help
```

## Features
- gRPC server implements one service with two functional methods `GetHealthChecks` and `StreamLogs`.
- Clients can invoke this method via REST proxy through: `http://127.0.0.1:8000/hc/<service>`
- Http server provides a ws endpoint for streaming files specified by client via body message

