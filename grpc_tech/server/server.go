package grpc_tech

import (
	"context"
	"errors"
	"fmt"
	"io"
	"log"
	"net"

	grpcpb "gitlab.com/golang-test/grpc_tech/proto"
	"gitlab.com/golang-test/watcher"
	"google.golang.org/grpc"
)

type server struct {
	grpcpb.UnimplementedTestServiceServer
}

type HealthCheckInfo struct {
	Service string
	Check   string
}

var healthchecks = make(map[string]HealthCheckInfo)

func init() {
	// Set initial dummy data
	healthchecks["redis"] = HealthCheckInfo{
		Service: "redis",
		Check:   "passing",
	}

	healthchecks["k8s-prov"] = HealthCheckInfo{
		Service: "redis",
		Check:   "passing",
	}

	healthchecks["nbm"] = HealthCheckInfo{
		Service: "nbm",
		Check:   "down",
	}
}

func (s *server) GetHealthChecks(ctx context.Context, req *grpcpb.HealthCheckRequest) (*grpcpb.HealthCheckInfo, error) {
	log.Printf("Getting healthchecks for %v", req.Service)
	hc, found := healthchecks[req.Service]
	if found {
		return &grpcpb.HealthCheckInfo{
			Service: hc.Service,
			Check:   hc.Check,
		}, nil
	}
	return nil, errors.New("Service not found")
}

func (s *server) StreamLogs(req *grpcpb.StreamLogsRequest, stream grpcpb.TestService_StreamLogsServer) error {
	filePath := req.File
	log.Printf("Streaming %v", filePath)
	dataChan := make(chan string)
	errChan := make(chan error)
	defer close(dataChan)
	defer close(errChan)
	go watcher.WatchFile(filePath, dataChan, errChan)
	for {
		select {
		case line := <-dataChan:
			fmt.Println(line)
			err := stream.Send(&grpcpb.StreamLogsResponse{
				Line: line,
			})
			if err != nil {
				fmt.Println("Could not send: ", err)
				return err
			}
		case eof := <-errChan:
			if eof == io.EOF {
				// return nil
			}
		}

	}
	return nil
}

func NewTestGrpsServer(port *int) {
	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", *port))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	s := grpc.NewServer()
	grpcpb.RegisterTestServiceServer(s, &server{})
	log.Printf("Grpc Server listening at %v", lis.Addr())
	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
